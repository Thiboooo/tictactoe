#include "Tictactoe.hpp"
#include <sstream>

using namespace std;

int main() {

    Jeu *j = new Jeu();
    ostringstream stream;
    stream << *j;
    std::string str = stream.str();
    cout << str;
    bool gagnant = false;
    int ligne , colonne ;
    cout << "\n"<< endl;
    while(gagnant == false){
        cout << "\nEntrez un coup (i j) pour rouge :"<< endl;
        cin >> ligne >> colonne ;
        cout << "\n"<< endl;
        j->jouer(ligne,colonne);
        cout << *j;
        cout << "\n___________________________"<< endl;
        if (j->getVainqueur() == 1){
            cout << "Le Joueur Rouge a Gagner !" << endl;
            break;
        }
        if (j->getVainqueur() == 2){
            cout << "Le Joueur Vert a Gagner !" << endl;
            break;
        }
        if (j->compteur == 8 && j->getVainqueur() == 0){
            cout << "Egalité !" << endl;
            break;
        }
    }
}