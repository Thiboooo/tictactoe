#include "Tictactoe.hpp"
using namespace std;


Jeu::Jeu() {
    raz();
    
}

void Jeu::raz(){
    std::cout << "initialisation du jeu" << std::endl;
    _grille = {JOUEUR_VIDE,JOUEUR_VIDE,JOUEUR_VIDE,JOUEUR_VIDE,JOUEUR_VIDE,JOUEUR_VIDE,JOUEUR_VIDE,JOUEUR_VIDE,JOUEUR_VIDE};
    
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    for(int i = 0; i < 9; i++){
            if(jeu._grille[i]==JOUEUR_VIDE){
                os << ".";
            }else if(jeu._grille[i]==JOUEUR_ROUGE){
                os << "R";
            }else if(jeu._grille[i]==JOUEUR_VERT){
                os<<"V";
            }
            if(i == 2){
                os<<"\n";
            }
            if(i == 5){
                os<<"\n";
            }
        }
    return os;
}

Joueur Jeu::getVainqueur() const {
    if ( _grille[0] == 1 && _grille[1] == 1 && _grille[2] == 1 || _grille[0] == 2 && _grille[1] == 2 && _grille[2] == 2 ) {
        return _grille[0];
    }else if (_grille[3] == 1 && _grille[4] == 1 && _grille[5] == 1 || _grille[3] == 2 && _grille[4] == 2 && _grille[5] == 2){
        return _grille[3];
    }else if ( _grille[6] == 1 && _grille[7] == 1 && _grille[8] == 1 || _grille[6] == 2 && _grille[7] == 2 && _grille[8] == 2){
        return _grille[6];
    }else if ( _grille[0] == 1 && _grille[4] == 1 && _grille[8] == 1 || _grille[0] == 2 && _grille[4] == 2 && _grille[8] == 2){
        return _grille[0];
    }else if ( _grille[2] == 1 && _grille[4] == 1 && _grille[6] == 1 || _grille[2] == 2 && _grille[4] == 2 && _grille[6] == 2){
        return _grille[2];
    }else if ( _grille[0] == 1 && _grille[3] == 1 && _grille[6] == 1 || _grille[0] == 2 && _grille[3] == 2 && _grille[6] == 2){
        return _grille[0];
    }else if ( _grille[1] == 1 && _grille[4] == 1 && _grille[7] == 1 || _grille[1] == 2 && _grille[4] == 2 && _grille[7] == 2){
        return _grille[1];
    }else if ( _grille[2] == 1 && _grille[5] == 1 && _grille[8] == 1 || _grille[2] == 2 && _grille[5] == 2 && _grille[8] == 2){
        return _grille[2];
    }else{
        return JOUEUR_VIDE;
    }   
}

Joueur Jeu::getJoueurCourant(){
    if (compteur % 2 == 0){
        compteur = compteur + 1 ;
        return JOUEUR_ROUGE;
    }else{
        compteur = compteur + 1 ;
        return JOUEUR_VERT;
    }
}

bool Jeu::jouer(int i, int j) {
    if ( i == 1 && j == 1 ){
        return _grille[0] = getJoueurCourant();
    }else if ( i == 1 && j == 2 ){
        return _grille[1] = getJoueurCourant();
    }else if ( i == 1 && j == 3 ){
        return _grille[2] = getJoueurCourant();
    }else if ( i == 2 && j == 1 ){
        return _grille[3] = getJoueurCourant();
    }else if ( i == 2 && j == 2 ){
        return _grille[4] = getJoueurCourant();
    }else if ( i == 2 && j == 3 ){
        return _grille[5] = getJoueurCourant();
    }else if ( i == 3 && j == 1 ){
        return _grille[6] = getJoueurCourant();
    }else if ( i == 3 && j == 2 ){
        return _grille[7] = getJoueurCourant();
    }else if ( i == 3 && j == 3 ){
        return _grille[8] = getJoueurCourant();
    }else{
        cout << "pas dans le plateau , rejoues !" << endl;

        return 0;
    }
    //return _grille[((i*j)-1)] = getJoueurCourant();
}

