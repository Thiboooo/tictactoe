//"use strict";

function joueurVersTexte(joueur) {
    switch (joueur) {
        case Module.Joueur.VIDE: return "vide";
        case Module.Joueur.ROUGE: return "rouge";
        case Module.Joueur.VERT: return "vert";
        case Module.Joueur.EGALITE: return "égalité";
    }
}

function joueurVersCss(joueur) {
    switch (joueur) {
        case Module.Joueur.ROUGE: return "a joueurRouge";
        case Module.Joueur.VERT: return "a joueurVert"
        default: return "a joueurVide";
    }
}

const Gui = function(jeu) {
    // initialise le jeu
    this.jeu = jeu;

    var tableau = document.createElement('table');
    console.log("test");
    for (let i = 0; i < 3; i++){
        var l = document.createElement('tr');
        for (let j = 0; j < 3;j++){
            var c = document.createElement('td');
            c.onclick = _ => this.play(i,j);
            c.id = i+''+j;
            l.appendChild(c);
        }
        tableau.appendChild(l);
    }
    document.body.appendChild(tableau);
    // crée le bouton
    const dom_bouton = document.createElement('button');
    dom_bouton.innerHTML = "Recommencer";
    dom_bouton.onclick = _ => this.gererBouton();
    document.body.appendChild(dom_bouton);

    // reinitialise le jeu et l'interface
    this.razGui();
};

Gui.prototype.gererBouton = function() {
    this.razGui();
};

Gui.prototype.razGui = function() {
    // ...
};

Gui.prototype.play = function(i,j){
    var c = document.getElementById(i+''+j);
    var joueur_courant= this.jeu.getJoueurCourant();
    if(this.jeu.jouer(i,j) == true){
        c.classList.add(joueurVersCss(joueur_courant));
    }
}

